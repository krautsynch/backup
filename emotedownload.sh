#!/bin/bash
i=0;
emotes=$(cat $1);
length=$(echo $emotes | jq length);
while [ $i -lt $length ]; do
  name=$(echo $emotes | jq -r .[$i].name);
  file=${name:1};
  for f in $file.*; do
    [[ -f $f ]] && echo "$f exists" && continue
    image=$(echo $emotes | jq -r .[$i].image);
    curl -Lk $image > $file;
    #${name:1}.${image##*.};
    if [ -f "$file" ]; then
      mime=$(file -b --mime-type $file);
      mime=${mime##*\/};
      if [ $mime == "jpeg" ]; then
        mime="jpg";
      fi
      mv $file $file.$mime && echo "$file.$mime downloaded";
    else
      echo "ERROR: $name";
    fi
  done
  i=$(($i+1));
done;
