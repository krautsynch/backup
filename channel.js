(function kr4ssiScripts() {

 const opt = [
  ['kr4-lightcase', 'Use Lightcase for image-embeds', true],
  ['kr4-allofthelights', 'Use Allofthelights for Cinemamode-Button', false],
  ['kr4-pasteimage', 'Upload images pasted into the Chatline', false],
  ['kr4-scalebutton', 'Show Scale-Button', true],
  ['kr4-importexport', 'Show Import/Export-Buttons', true],
  ['kr4-codemirror', 'Use Codemirror for editing (turning off needs reload)', false],
 ]

 const imagehost = 'https://cors-anywhere.herokuapp.com/https://uguu.se/api.php?d=upload-tool'

 const codemirror_options = {
  theme: 'twilight',
  lineNumbers: true,
  autoRefresh: true,
  matchTags: true,
  autoCloseTags: true,
  matchBrackets: true,
  autoCloseBrackets: true,
  showTrailingSpace: true,
  foldGutter: true,
  scrollbarStyle: 'overlay',
  gutters: ['CodeMirror-lint-markers', 'CodeMirror-linenumbers', 'CodeMirror-foldgutter'],
  lint: {
   asi: true,
   browser: true,
   esversion: 11,
   highlightLines: true,
   latedef: 'nofunc'
  },
  search: {
   bottom: true
  }
 }

 const beautify_options = {
  indent_size: 2,
  js: {
   indent_size: 1,
   brace_style: 'end-expand,preserve-inline'
  }
 }

 const lightcase_options = {
  transition: 'none'
 }

 const libs = (libMap = (pre, libs) => libs.trim().split(/\s+/).map(lib => pre + lib).join(' ')) => `
${opt.codemirror ? `https://cdn.jsdelivr.net/npm/tenko@1.1.1/build/tenko.prod.min.js
https://cdn.jsdelivr.net/npm/htmlhint@latest/dist/htmlhint.js` : ''}
${libMap('https://cdnjs.cloudflare.com/ajax/libs/', `
 ${opt.importexport ? `jszip/3.5.0/jszip.min.js` : ''}
 ${opt.codemirror ? `js-beautify/1.13.5/beautifier.min.js
 jshint/2.13.1/jshint.min.js
 csslint/1.0.5/csslint.min.js
 ${libMap('codemirror/5.63.0/', `
  codemirror.min.js
  codemirror.min.css
  theme/twilight.min.css
  ${libMap('mode/', `
   javascript/javascript.min.js
   xml/xml.min.js
   css/css.min.js
   meta.min.js
  `)}
  ${libMap('addon/', `
    display/autorefresh.min.js
   ${libMap('dialog/', `
    dialog.min.js
    dialog.min.css
   `)}
   ${libMap('search/', `
    searchcursor.min.js
    search.min.js
    matchesonscrollbar.min.js
    matchesonscrollbar.min.css
    match-highlighter.min.js
   `)}
   ${libMap('edit/', `
    matchbrackets.min.js
    closebrackets.min.js
    matchtags.min.js
    trailingspace.min.js
    closetag.min.js
   `)}
   ${libMap('fold/', `
    brace-fold.min.js
    comment-fold.min.js
    foldcode.min.js 
    foldgutter.min.css
    foldgutter.min.js
    xml-fold.min.js
   `)}
   ${libMap('lint/', `
    lint.min.js
    lint.min.css
    javascript-lint.min.js
    css-lint.min.js
    html-lint.min.js
   `)}
   ${libMap('scroll/', `
    annotatescrollbar.min.js
    simplescrollbars.min.js
    simplescrollbars.min.css
   `)}
  `)}
 `)}`: ''}
 ${opt.lightcase ? `${libMap('lightcase/2.5.0/', `
  js/lightcase.min.js
  css/lightcase.min.css
 `)}`: ''}
 ${opt.allofthelights ? 'Allofthelights.js/2.0/jquery.allofthelights-min.js' : ''}
`)}`.trim().split(/\s+/)

 function loadLibs(libs) {
  return libs.reduce((promise, src) => promise.then(() => {
   let e
   if (src.endsWith('.js')) {
    if ($(`script[src="${src}"]`).length) return
    e = $('<script />', { src })[0]
   }
   else {
    if ($(`link[href="${src}"]`).length) return
    e = $('<link>', {
     rel: 'stylesheet',
     type: 'text/css',
     href: src
    })[0]
   }
   return new Promise((resolve, reject) => {
    e.addEventListener('load', resolve)
    e.addEventListener('error', reject)
    document.head.appendChild(e)
   })
  }), Promise.resolve())
 }

 function selectFile(callback = () => null, accept = '') {
  $(`<input type="file" accept="${accept}" />`).change(e => {
   callback(e.target.files[0])
  }).click()
 }

 function saveBlob(blob, name) {
  const split = name.split('.')
  const ext = split.pop()
  const href = URL.createObjectURL(blob)
  $('<a />', {
   href,
   download: `${split.join('.')}_${CHANNEL.name}_${Date.now()}.${ext}`,
  })[0].click()
  setTimeout(() => window.URL.revokeObjectURL(href), 0)
 }

 const beautyJson = data => JSON.stringify(data, null, 2)

 const parseJson = (text, def = '[]') => JSON.parse(text.trim() ? text : def)

 const emotesJson = () => beautyJson(CHANNEL.emotes.map(({ name, image }) => ({ name, image })))

 const requestFilterJson = () => new Promise((resolve, reject) => {
  if (CLIENT.rank < CHANNEL.perms.filteredit) return reject('No permission to read Chatfilters')
  socket.once('chatFilters', resolve)
  socket.emit('requestChatFilters')
 }).then(beautyJson)
 const id = 'us-kr4ssiscripts'

 const chkBool = val => typeof val === 'boolean' ? 'checked' : 'value'

 $(`#${id}`).remove()
 $(`a[href="#${id}"`).parent().remove()
 $('#useroptions .tab-content').append($('#us-mod').clone().attr({ id }).append(function() {
  const entries = opt.map(([key, label, val]) => {
   return $(this).find('label:first').attr('for', key).text(label).prepend($('<input />', {
    id: key,
    type: typeof val === 'boolean' ? 'checkbox' : 'input',
    [chkBool(val)]: opt[key.slice(4)] = getOrDefault(key, val)
   })).closest('.form-group').clone()
  })
  $(this).children('h4').text('kr4ssiScript Options')
  return $(this).children('form').empty().append(entries)
 }))
 $('#useroptions ul.nav').append($('<li />').append($('<a />', {
  'data-toggle': 'tab',
  href: `#${id}`,
  text: 'Channel Options'
 })))

 $('#videocontrols button[title="Scale the video player"]').remove()
 if (opt.scalebutton) $('#videocontrols').prepend($('<div class="btn-group dropdown" />')
  .append($('<button class="btn btn-default btn-sm dropdown-toggle" title="Scale the video player" data-toggle="dropdown" />')
   .html('<span class="glyphicon glyphicon-resize-horizontal"/><span class="caret" />'))
  .append($('<ul class="dropdown-menu ul-double"/>').append(...Object.entries({
   'Default': 1,
   '16:9 -> 4:3': 0.75,
   '4:3 -> 16:9': 1.333333333
  }).map(([text, scale]) => $('<li class="li-double" />').append($('<a />').text(text).click(() => {
   $('#ytapiplayer').css({
    'transform': `scaleX(${scale})`,
    '-webkit-transform': `scaleX(${scale})`
   })
  }))))))

 window.kr4ssiScripts = window.kr4ssiScripts || {}

 window.kr4ssiScripts.oldSaveUserOptions = window.kr4ssiScripts.oldSaveUserOptions || saveUserOptions
 saveUserOptions = () => {
  opt.forEach(([key, , val]) => setOpt(key, $(`#${key}`).prop(chkBool(val))))
  window.kr4ssiScripts.oldSaveUserOptions()
  kr4ssiScripts()
 }

 window.kr4ssiScripts.oldFormatURL = window.kr4ssiScripts.oldFormatURL || window.formatURL
 window.formatURL = data => data.type != 'cu' ? window.kr4ssiScripts.oldFormatURL(data) : data.meta.embed.src

 if (typeof window.kr4ssiScripts.pasteImage === 'function') $('#chatline').off('paste', window.kr4ssiScripts.pasteImage)
 if (opt.pasteimage) $('#chatline').on('paste', window.kr4ssiScripts.pasteImage = e => {
  [...e.originalEvent.clipboardData.items].forEach(item => {
   if (!item.type.startsWith('image')) return
   const body = new FormData()
   body.append('file', item.getAsFile())
   fetch(imagehost, {
    method: 'POST',
    body
   }).then(res => {
    if (!res.ok) throw new Error('Network error.')
    return res.text()
   }).then(res => {
    e.target.value = res + '.pic'
   }).catch(alert)
  })
 })

 window.module = window.module || {} //workaround
 return loadLibs(libs()).then(() => {

  $('#channeloptions .nav-tabs a.btn:contains("port Backup")').parent().remove()
  if (opt.importexport) $('#channeloptions .nav-tabs').append([
   $('<li />').append($('<a class="btn">Export Backup</a>').click(() => {
    const zip = new JSZip()
     .file('perms.json', beautyJson(CHANNEL.perms))
     .file('opts.json', beautyJson(CHANNEL.opts))
     .file('emotes.json', emotesJson())
     .file('motd.html', CHANNEL.motd)
     .file('css.css', CHANNEL.css)
     .file('js.js', CHANNEL.js)
    requestFilterJson().then(filters => {
     zip.file('filters.json', filters)
    }).catch(alert).then(() => zip.generateAsync({ type: 'blob' })).then(content => {
     saveBlob(content, 'backup.zip')
    }).catch(alert)
   })), $('<li />').append($('<a class="btn">Import Backup</a>').click(() => {
    selectFile(file => new JSZip().loadAsync(file).then(({ files }) => Object.entries({
     'perms.json': perms => socket.emit('setPermissions', parseJson(perms, '{}')),
     'opts.json': opts => socket.emit('setOptions', parseJson(opts, '{}')),
     'emotes.json': emotes => socket.emit('importEmotes', parseJson(emotes)),
     'motd.html': motd => socket.emit('setMotd', { motd }),
     'css.css': css => socket.emit('setChannelCSS', { css }),
     'js.js': js => socket.emit('setChannelJS', { js }),
     'filters.json': filters => socket.emit('importFilters', parseJson(filters))
    }).forEach(([filename, read]) => () => (async () => {
     return files[filename].async('text').then(read)
    })().catch(err => alert(filename + ': ' + err)))).catch(alert), 'multipart/x-zip')
   }))
  ])

  $('#videocontrols > .switch').remove()
  if (opt.allofthelights) {
   if (!$('.fluid_width_video_wrapper').length) $('#videowrap > .embed-responsive').allofthelights()
   $('#videocontrols').prepend($('<div class="btn btn-sm btn-default switch" title="Cinema-mode" />')
    .html('<span class="glyphicon glyphicon-film"/>'))
  }

  if (typeof window.kr4ssiScripts.lightCase === 'function') socket.off('chatMsg', window.kr4ssiScripts.lightCase)
  if (opt.lightcase) {
   $('#messagebuffer').find('.image-embed-small').parent().lightcase(lightcase_options)
   socket.on('chatMsg', window.kr4ssiScripts.lightCase = () => {
    $('#messagebuffer').children().last().find('.image-embed-small').parent().lightcase(lightcase_options)
   })
  }

  if (CLIENT.loaded_kr4ssiscripts === true || !opt.codemirror) return
  CLIENT.loaded_kr4ssiscripts = true

  class Editor {
   constructor(btn, filename = '', options = {}) {
    this.name = btn.siblings('h4').text().replace(/ editor$/, '')
    const textarea = btn.siblings('textarea')[0]
    const scroll = textarea.scrollTop / textarea.scrollHeight
    this.cm = CodeMirror.fromTextArea(textarea, {
     ...codemirror_options,
     mode: CodeMirror.findModeByFileName(filename)?.mime,
     ...options
    })
    //this.cm.scrollTo(0, this.cm.getScrollInfo().height * scroll)
    this.cm.setOption('extraKeys', {
     'Ctrl-Enter': () => this.import()
    })
    this.cm.on('scroll', cm => {
     textarea.scrollTop = textarea.scrollHeight * (this.cm.getScrollInfo().top / this.cm.getScrollInfo().height)
    })
    btn.prev('button').off('click').click(() => {
     this.export().then(data => this.setValue(data)).catch(alert)
    })
    btn.off('click').click(() => {
     this.import().catch(alert)
    })
    btn.after(btn.clone().attr('id', '').text(`Import ${this.name} from file`).click(() => {
     selectFile(file => file.text().then(text => this.importFile(text)).catch(alert), this.cm.options.mode)
    }))
    btn.after(btn.clone().attr('id', '').text(`Export ${this.name} to file`).click(() => {
     this.export().then(data => saveBlob(new Blob([data], { type: this.cm.options.mode }), filename)).catch(alert)
    }))
   }
   async validate() {
    this.cm.performLint()
    for (let { __annotation } of this.cm.state.lint.marked) {
     if (__annotation.severity === 'error') {
      this.cm.scrollIntoView(__annotation.from)
      throw __annotation.message
     }
    }
    return this.cm.getValue()
   }
   async export () {
    return this.cm.getValue()
   }
   setValue(value) {
    const top = this.cm.getScrollInfo().top
    //const cur = this.cm.getCursor()
    this.cm.setValue(value)
    this.cm.scrollTo(0, top)
    //this.cm.setCursor(cur)
   }
   importFile(data) {
    this.setValue(data)
    this.import()
   }
   import(data = this.cm.getValue()) {
    return this.validate(data).then(data => this.save(data))
   }
   save() {}
  }

  class JsonEditor extends Editor {
   async validate(data) {
    if (!confirm(`You are about to import ${this.name} from file, or the contents of the textbox below the import button.
If this is empty, it will clear all of your filters. Are you sure you want to continue?`)) throw 'aborted'
    return parseJson(data)
   }
   importFile(data) {
    return super.import(data).catch(() => {
     this.setValue(data)
     return super.validate()
    })
   }
   import() {
    return super.import().catch(() => super.validate())
   }
  }

  class CodeEditor extends Editor {
   constructor(btn, filename) {
    super(...arguments)
    this.maxLength = 20000
    this.btfy = beautifier[filename.split('.').pop()]
    const left = $('<span />').appendTo(btn.siblings('p'))
    const charsleft = () => left.text(` ${this.maxLength - this.cm.getValue().length} chars left`)
    charsleft()
    this.cm.on('change', charsleft)
    btn.after(btn.clone().attr('id', '').text('Beautify').click(() => this.beautify()))
   }
   beautify() {
    const code = this.btfy(this.cm.getValue(), beautify_options)
    if (code.length > this.maxLength) alert('Beautified code exceeds 20KB')
    else this.setValue(code)
   }
  }

  new class extends JsonEditor {
   export () {
    return requestFilterJson()
   }
   save(data) {
    socket.emit('importFilters', data)
   }
  }($('#cs-chatfilters-import'), 'filters.json')

  new class extends JsonEditor {
   export () {
    return Promise.resolve(emotesJson())
   }
   save(data) {
    socket.emit('importEmotes', data)
   }
  }($('#cs-emotes-import'), 'emotes.json')

  const motdtext = new class extends CodeEditor {
   save(motd) {
    socket.emit('setMotd', { motd })
   }
  }($('#cs-motdsubmit'), 'motd.html')

  const csstext = new class extends CodeEditor {
   save(css) {
    socket.emit('setChannelCSS', { css })
   }
  }($('#cs-csssubmit'), 'css.css')

  const jstext = new class extends CodeEditor {
   save(js) {
    Tenko(js)
    socket.emit('setChannelJS', { js })
   }
  }($('#cs-jssubmit'), 'js.js', {
   type: 'application/javascript',
   indentUnit: 1,
  })

  socket.on('channelCSSJS', ({ css, js }) => {
   jstext.setValue(js)
   csstext.setValue(css)
  })

  socket.on('setMotd', motd => {
   motdtext.setValue(motd)
  })

 })
})().catch(console.error)

if (!CLIENT.loaded_krautscripts) {
 // **************************
 // KRAUTSCRIPTS (c) Berndboy2
 // **************************
 CLIENT.loaded_krautscripts = true;

 function checkVideosCount(data) {
  console.log("checkVideoCountData", data);
  setTimeout(function() {
   var myUsername = $('#welcome').text().replace("Welcome, ", "");
   var ownVideosCount = $('#queue').children("[title='Added by: " + myUsername + "']").length;
   ownVideosCount += $('.videolist').children("[data-original-title='Added by: " + myUsername + "']").length;
   console.log("myUsername", myUsername, "ownVideosCount", ownVideosCount);
   $('#mediaurl').attr('placeholder', 'Derzeit ' + ownVideosCount + " Videos von dir in der Liste");
  }, 500);
 };
 socket.on("playlist", function(data) {
  checkVideosCount(data);
 });
 socket.on("queue", function(data) {
  checkVideosCount(data);
 });
 socket.on("delete", function(data) {
  checkVideosCount(data);
 });


 //Kinomodus
 $(document).ready(function() {
  $("#usercount").after("<button id='cinemamode' class='btn btn-sm btn-default cinemamodeoff'>Kinomodus</button>");
 });

 $('#cinemamode').bind('click', function() {
  $('body').toggleClass('cinemamodeon');
 });
}

// Xaekai's clickable emotes

$("#messagebuffer").unbind("click.transmote");
$("#messagebuffer").on("click.transmote", "img.channel-emote", function(ev) {
 function _snd(snd, vol) {
  snd.volume = VOLUME + (VOLUME / 10);
  return snd;
 }

 switch ($(this).attr("title")) {

  case '/uguu':
   window[CHANNEL.name].uguu.play();
   break;
  case '/tree':
   _snd(new Audio('https://resources.pink.horse/sounds/tree.niggers.ogg'), .66).play();
   break;
  case '/kdurch':
   _snd(new Audio('https://b.hitlers.kz/pfqqep.ogg'), .66).play();
   break;
  case '/tkalli':
   _snd(new Audio('https://b.hitlers.kz/lwoyqj.ogg'), .66).play();
   break;
  case '/fridimett':
   _snd(new Audio('https://b.hitlers.kz/wslpxy.ogg'), .66).play();
   break;
  case '/3tanzi':
   _snd(new Audio('https://b.hitlers.kz/cowtwp.ogg'), .66).play();
   break;
  case '/wirdgebaut':
   _snd(new Audio('https://files.catbox.moe/ai198e.ogg'), .66).play();
   break;
  case '/honk':
   _snd(new Audio('https://files.catbox.moe/r8m3i5.ogg'), .66).play();
   break;
  case '/feslglaun':
   _snd(new Audio('https://files.catbox.moe/dtpbh6.mp3'), .66).play();
   break;
  case '/3feslglaun':
   _snd(new Audio('https://files.catbox.moe/dtpbh6.mp3'), .66).play();
   break;
  case '/8lamentierend':
   _snd(new Audio('https://www.soundjay.com/nature/rain-02.mp3'), .66).play();
   break;
 }

})

if (this[CHANNEL.name] === undefined) {
 this[CHANNEL.name] = {}
}

// -- Uguu is special --
this[CHANNEL.name].uguu = {}
this[CHANNEL.name].uguu.generate = function() {
 var base_url = "https://resources.pink.horse/sounds/uguu/"
 var choice = Math.floor(Math.random() * 85) + 1;
 this.current = new Audio(base_url + 'uguu.' + (() => {
  if (choice < 10) {
   return ("0" + choice)
  }
  else return choice
 })() + '.ogg')
}
this[CHANNEL.name].uguu.play = function() {
 this.generate();
 this.current.volume = VOLUME + (VOLUME / 5)
 this.current.play()
}