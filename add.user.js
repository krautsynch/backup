// ==UserScript==
// @name ponkbot .add
// @namespace https://github.com/kr4ssi/PonkBot/
// @version 0.7.1.1.0.7
// @author kr4ssi
// @include /XFileShareIE/
// @include /https?:\/\/(?:www\.)?streamtape\.com\/[ve]\/([^/?#&]+)/
// @include /https?:\/\/(?:www\.)?voe\.sx(?:\/e)?\/([^/?#&]+)/
// @include /https?:\/\/(?:www\.)?videobin\.co\/(?:embed-)?([^/?#&]+)/
// @include /VivoIE/
// @include /https?:\/\/(?:www\.)?vidoza\.(?:net|org|co)\/(?:embed-)?([^/?#&]+)/
// @include /https?:\/\/(?:www\.)?2ix2\.com\/.+/
// ==/UserScript==

const includes = [{
    regex: /XFileShareIE/,
    groups: [],
    init: function() {
      let e = document.querySelector('video')
      e = document.querySelector('video').firstElementChild || e
      e = holaplayer && holaplayer.cache_ || e
      if (!e) return false
      this.fileurl = e.src
    }
  },
  {
    regex: /https?:\/\/(?:www\.)?streamtape\.com\/[ve]\/([^/?#&]+)/,
    groups: ["id"],
    init: function() {
      return this.bot.fetch(this.url, {
        match: new RegExp(/<meta name="og:title" content="([^"]*)[\s\S]+/.source +
          /olink" style="display:none;">([^<]+)/.source)
      }).then(({
        body,
        match: [, title, fileurl]
      }) => {
        const url = new URL('https:/' + fileurl)
        const token = url.searchParams.get('token')?.slice(8)
        const match = body.match(new RegExp(`${token}[^']+(?!${token})`))
        if (!match) throw new Error('no match \'' + match + '\' found')
        url.searchParams.set('token', match[0])
        url.searchParams.set('stream', 1)
        console.log(match, url)
        return Object.assign(this, {
          title,
          fileurl: url.toString()
        })
      })
    }
  },
  {
    regex: /https?:\/\/(?:www\.)?voe\.sx(?:\/e)?\/([^/?#&]+)/,
    groups: ["id"],
    init: function() {
      this.matchUrl(this.url.replace(/\/e/i, ''))
      return this.bot.fetch(this.url, {
        match: /<title>Watch ([^<]*)[\s\S]+\"hls\": \"([^"]+)/
      }).then(({
        match: [, title, fileurl]
      }) => {
        return Object.assign(this, {
          title,
          fileurl
        })
      })
    }
  },
  {
    regex: /https?:\/\/(?:www\.)?videobin\.co\/(?:embed-)?([^/?#&]+)/,
    groups: ["id"],
    init: function() {
      this.matchUrl(this.url.replace(/embed-/i, ''))
      return this.bot.fetch(this.url, {
        match: /<title>\s+Watch ([^\n]*)[\s\S]+sources: \[\"([^"]+)/
      }).then(({
        match: [, title, fileurl]
      }) => {
        return Object.assign(this, {
          title,
          fileurl
        })
      })
    }
  },
  {
    regex: /VivoIE/,
    groups: [],
    init: function() {
      const match = document.documentElement.outerHTML.match(/\n\t\t\tsource: '([^']+)/)
      if (!match) return false
      this.fileurl = function r(a, b) {
        return ++b ?
          String.fromCharCode((a = a.charCodeAt() + 47, a > 126 ? a - 94 : a)) :
          a.replace(/[^ ]/g, r)
      }(decodeURIComponent(match[1]))
    }
  },
  {
    regex: /https?:\/\/(?:www\.)?vidoza\.(?:net|org|co)\/(?:embed-)?([^/?#&]+)/,
    groups: ["id"],
    init: function() {
      this.matchUrl(this.url.replace(/embed-/i, '').replace(/\.html$/, ''))
      return this.bot.fetch(this.url, {
        match: /([^"]+\.mp4)[\s\S]+vid_length: '([^']+)[\s\S]+curFileName = "([^"]+)/
      }).then(({
        match: [, fileurl, duration, title]
      }) => {
        return Object.assign(this, {
          title,
          fileurl,
          duration: parseFloat(duration)
        })
      })
    }
  },
  {
    regex: /https?:\/\/(?:www\.)?2ix2\.com\/.+/,
    groups: [],
    init: function() {
      let match
      document.querySelectorAll('script').forEach(e => {
        match = e.textContent.match(/file: "([^"]+)/) || match
      })
      if (!match) return false
      this.fileurl = match[1]
    }
  }]

const config = Object.assign({
  weblink: 'https://becci.krautsynch.de',
  bot: {
    fetch(url, opt, r) {
      return function fetch(url, {
        qs = {},
        form = false,
        method = 'get',
        json = true,
        jsonparse = false,
        body = '',
        getprop = false,
        getlist = false,
        getrandom = false,
        match = false,
        customerr = [],
        headers = {},
        cloud = false,
        $ = false,
        unpack = false,
        onCaptcha = false,
        lookup = undefined
      } = {}, r = () => new Promise((resolve, reject) => {
        const r = cloud ? cloudscraper : request
        r(Object.assign({
          headers: Object.assign({
            'User-Agent': (new UserAgent()).toString()
          }, headers),
          url,
          qs,
          form,
          method,
          json //: match ? false : json
        }, body && {
          body
        }, onCaptcha && {
          onCaptcha
        }, lookup && {
          lookup
        }), (err, res) => {
          if (err) reject(err)
          else resolve(res)
        })
      })) {
        if ((getlist || getprop) && !json && !jsonparse) throw new Error('json or jsonparse must set to true')
        if (getrandom && !getlist) throw new Error('getrandom from where')
        class matchError extends Error {}
        console.log('Fetch:', ...arguments)
        return r().then((res, body = res.body) => {
          if (jsonparse) body = JSON.parse(body)
          if (body && body.err) throw body.err
          const result = {
            body,
            statusCode: res.statusCode,
            headers,
            res
          }
          if (res.statusCode != 200 && !customerr.includes(res.statusCode)) {
            console.error(body)
            throw new matchError(res.statusCode)
          }
          if (getprop && !body[getprop]) throw new matchError(`no property '${getprop}' found`)
          result.prop = (body && body[getprop]) || body
          if (getlist) {
            if (!result.prop[getlist] || result.prop[getlist].length < 1) throw new matchError('no list \'' + getlist + '\' found')
            result.list = result.prop[getlist]
            if (getrandom) result.random = result.list[Math.floor(Math.random() * result.list.length)]
          }
          if (match) {
            result.match = result.prop.match(match)
            if (!result.match) {
              console.error(body)
              throw new matchError('no match \'' + match + '\' found')
            }
          }
          if (unpack) {
            function unPack(p, a, c, k, e, d) {
              while (c--)
                if (k[c]) p = p.replace(new RegExp('\\b' + c.toString(a) + '\\b', 'g'), k[c]);
              return p
            }
            const regex = /return p}\('(.*)',(\d+),(\d+),'(.*)'\.split\('\|'\)/g
            let match = regex.exec(result.prop)
            if (!match) {
              //console.error(body)
              throw new matchError('no packed code found')
            }
            while (match && !result.unpack) {
              let unpacked
              try {
                unpacked = unPack(match[1], match[2], match[3], match[4].split('|'))
                console.log(unpacked)
                result.unpack = unpacked.match(unpack)
              } catch (err) {
                console.log(err)
              }
              match = regex.exec(result.prop)
            }
            if (!result.unpack) {
              //console.error(body)
              throw new matchError('no match \'' + unpack + '\' in packed code found')
            }
          }
          if ($ && result.prop) result.$ = cheerio.load(result.prop)
          return result
        }).catch(err => {
          err instanceof matchError ? ponk.sendMessage(/\d/.test(err.message) ? ('Status: ' + err.message) : 'Keine Ergebnisse /elo') : console.error(err)
          throw err
        })
      }(url, opt, () => Promise.resolve({
        statusCode: 200,
        body: document.documentElement.outerHTML
      }))
    }
  }
}, {})

console.log('Userscript loaded', new class UserScript {
  constructor(url) {
    const match = window.location.hash.match(/#userscript(\d+)?/)
    Object.assign(this, {
      active: !!match
    }, config, this.matchUrl(url))
    if (typeof this.init != 'function') throw new Error('No constructor found')
    if (!this.active) return
    this.start().then(() => {
      if (!this.fileurl) return
      if (this.postMessage && window.parent) return window.parent.postMessage({
        userlink: this.fileurl
      }, 'https://cytu.be/r/' + this.chan)
      if (this.useGetValue) return GM_setValue(this.url, this.fileurl)
      if (this.doAsk && !confirm(`Userlink:\n${this.fileurl}\nfür Addierungslink: ${this.url}\ngefunden. Dem Bot schicken?`)) return
      window.location.replace(`${this.weblink}/add.json?${new URLSearchParams({
              url: match[1],
              userlink: this.fileurl
            }).toString()}`)
    }).catch(console.error)
  }
  get url() {
    return this.match[0]
  }
  matchUrl(url, providerList = includes) {
    if (!providerList) throw new Error('No providerlist found')
    return providerList.find(provider => {
      return !!(this.match = url.match(provider.regex))
    })
  }
  async start() {
    if (await this.init() != false) return
    await new Promise(resolve => setTimeout(resolve, 1000))
    return start()
  }
}(window.location.href))